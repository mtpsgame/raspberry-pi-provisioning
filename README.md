# Raspberry Pi Provisioning

A script to install all dependencies I require on a Raspberry Pi (Raspbian OS) to run my applications. Should be executed using `sudo` unless root privileges are obtained.
